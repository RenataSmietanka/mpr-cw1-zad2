package pl.edu.pjwstk;

/**
 * Created by renata.smietanka on 2017-10-03.
 */
import java.util.Date;

public class Episode {

    private String name;
    private int duration;
    private int episodeNumber;
    private Date releaseDate;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getDuration() {
        return duration;
    }
    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Date getFirstDateOfDistribution() {
        return releaseDate;
    }
    public void setFirstDateOfDistribution(Date firstDateOfDistribution) {
        this.releaseDate = firstDateOfDistribution;
    }
    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public void setEpisodeNumber(int episodeNumber) {
        this.episodeNumber = episodeNumber;
    }

}

