package pl.edu.pjwstk;

/**
 * Created by renata.smietanka on 2017-10-03.
 */
import java.util.Date;

public class Actor {

    private String firstName;
    private String lastName;
    private Date birthDate;
    private String biography;

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public Date getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    public String getBiography() {
        return biography;
    }
    public void setBiography(String biography) {
        this.biography = biography;
    }

}
