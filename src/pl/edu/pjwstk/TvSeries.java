package pl.edu.pjwstk;

import java.util.List;

/**
 * Created by renata.smietanka on 2017-10-03.
 */
public class TvSeries {
    private List<Season> seasons;
    public List<Season> getSeasons() {
        return seasons;
   }
    public void setSeasons(List<Season> seasons) {
       this.seasons = seasons;
   }

    private List<Actor> actors;
    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    private Director director;
    public Director getDirector() {
        return director;
    }
    public void setDirector(Director director) {
        this.director = director;
    }

    private String name;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}