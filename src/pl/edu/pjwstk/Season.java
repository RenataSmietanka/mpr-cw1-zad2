package pl.edu.pjwstk;

import java.util.List;

/**
 * Created by renata.smietanka on 2017-10-03.
 */
public class Season {

    private int seasonNumber;
    private int yearOfRelease;
    private List<Episode> episods;

    public int getSeasonNumber() { return seasonNumber; }
    public void setSeasonNumber(int seasonNumber) { this.seasonNumber = seasonNumber; }

    public int getYearOfRelease() {
        return yearOfRelease;
    }
    public void setYearOfRelease(int yearOfRelease) { this.yearOfRelease = yearOfRelease; }

    public List<Episode> getEpisods() { return episods; }
    public void setEpisods(List<Episode> episods) { this.episods = episods; }
}